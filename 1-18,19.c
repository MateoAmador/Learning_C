#include <stdio.h>
#define MAXLINE 1000

int getlin(char line[], int maxline);
void copy(char to[], char from[]);
void reverse(char x[], int y);

int main()
{
  int len;
  int max;
  char line[MAXLINE];
  char longest[MAXLINE];

  max = 0;
  while ((len = getlin(line, MAXLINE)) > 0)
    {
      if (len > max)
	{
	  max = len;
	  copy(longest, line);
	}
    }
  if (max > 0)
    {
      reverse(longest, len);
    }
  return 0;
}


int getlin(char s[], int lim)
{
  int c, i;

  for (i = 0; i < lim-1 && (c = getchar()) != EOF && c != '\n' && c != ' ' && c != '\t'; ++i) //no trailing blanks or tabs, or empty lines
    {

      s[i] = c;
    }
  if (c == '\n')
    {
      s[i] = c;
      ++i;
    }
  s[i] = '\0';
  return i;
}


void copy(char to[], char from[])
{
  int i;

  i = 0;
  while ((to[i] = from[i]) != '\0')
    {
      ++i;
    }
}

void reverse(char x[], int y)
{
  int i;
  i = 5;
  while (i != 0)
    {
      printf("%c", x[i]);
      --i;
    }
}
