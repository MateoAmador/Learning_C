#include <stdio.h>
#include <string.h>

#define size 1000

void strrindex(int t, char s[]);

int main()
{
  int x = 'a'; //what to look for
  char y[size] = "ababbabbahcasbcaowhfasbammm"; //where to look in 
  strrindex(x, y);
}


void strrindex(int t, char s[])
{
  int x = -1;
  int i = 0;
  while (i != (strlen(s)) - 1)
    {
      if (s[i] == t)
	{
	  x = i;
	}
      i++;
    }
  printf("%d\n", x);
}
