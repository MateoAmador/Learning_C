#include <stdio.h>

void escape(char * s, char * t);
void unescape(char * s, char * t);

int main()
{
  char list1[100] = "Hello!\n\tI am Mateo Amador and I think a lot of things.\nThis has been my TED Talk.\n";
  char list2[100];

  printf("Original: \n%s\n", list1);

  escape(list2, list1);
  printf("Escaped: \n%s\n", list2);

  unescape(list1, list2);
  printf("Unescaped: \n%s\n", list1);
}

void escape(char * s, char * t)
{
  int i, j;
  i = j = 0;
  
  while (t[i])
    {
      switch(t[i])
	{
	case '\n':
	  s[j++] = '\\';
	  s[j] = 'n';
	  break;
	case '\t':
	  s[j++] = '\\';
	  s[j] = 't';
	  break;
	default:
	  s[j] = t[i];
	  break;
	}
      ++i;
      ++j;
    }
  s[j] = t[i];
}

void unescape(char * s, char * t)
{
  int i, j;
  i = j = 0;

  while (t[i])
    {
      switch(t[i])
	{
	case '\\':
	  switch(t[++i])
	    {
	    case 'n':
	      s[j] = '\n';
	      break;
	    case 't':
	      s[j] = '\t';
	      break;
	    default:
	      s[j++] = '\\';
	      s[j] = t[i];
	    }
	  break;
	default:
	  s[j] = t[i];
	}
      ++i;
      ++j;
    }
  s[j] = t[i];
}
