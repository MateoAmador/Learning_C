#include <stdio.h>
#include <string.h>

#define size 1000

void itob(int n, char s[], double b);

int main()
{
  double n = 1983746;
  double b = 16;
  char s[size];
  itob(n, s, b);
}


void itob(int n, char s[], double b)
{
  int i = 0;
  double dec = 0;
  int p = 1;
  int r[5];
  int x = 0;
  int y;
  int h;
  if (n < 10)
    {
      s[i] = n;
      printf("%d\n", s[0]);
    }
  else if (n >= 10)
    {
      while (p > 0)
	{
	  dec = (n / b);
	  p = dec;
	  dec = (dec - p);
	  r[x] = (dec * b);
	  n = p;
	  if (r[x] == 10)
	    {
	      r[x] = 'a';
	    }
	  else if (r[x] == 11)
	    {
	      r[x] = 'b';
	    }
	  else if (r[x] == 12)
	    {
	      r[x] = 'c';
	    }
	  else if (r[x] == 13)
	    {
	      r[x] = 'd';
	    }
	  else if (r[x] == 14)
	    {
	      r[x] = 'e';
	    }
	  else if (r[x] == 15)
	    {
	      r[x] = 'f';
	    }
	  x++;
	}
      while (r[x], x--)
	{
	  if (r[x] == 'a' || r[x] == 'b' || r[x] == 'c' || r[x] == 'd'
	      || r[x] == 'e' || r[x] == 'f')
	    {
	      printf("%c", r[x]);
	    }
	  else
	    {
	      printf("%d", r[x]);
	    }
	}
      printf("\n");
    }
}

/* while (p != 0) */
/* 	{ */
/* 	  dec = (n / b); */
/* 	  p = dec; */
/* 	  dec = (dec - p); */
/* 	  r = (dec * b); */
/* 	  if (p == 0) */
/* 	    { */
/* 	      printf("hey\n"); */
/* 	    } */
/* 	} */
