#include <stdio.h>
#include <string.h>

void expand(char s1[], char s2[]);

int main()
{
  char *s[] = {"a-z-", "z-a-", "-1-6-", "a-ee-a",
    "a-R-L", "1-9-1", "5-5", NULL};
  char expanded[100];
  int i = 0;
  while (s[i])
    {
      expand(expanded, s[i]);
      printf("Original: \n%s\n", s[i]);
      printf("Expanded: \n%s\n", expanded);
      ++i;
    }
  return 0;
}


void expand(char s1[], char s2[])
{
  char upper_alpha[26] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  char lower_alpha[26] = "abcdefghijklmnopqrstuvwxyz";
  char digits[10] = "0123456789";

  char * start, * finish, * p;
  int i, j;
  i = j = 0;

  while (s2[i])
    {
      switch(s2[i])
	{
	case'-':
	  if (i == 0 || s2[i + 1] == '\0')
	    {
	      s1[j++] = '-';
	      ++i;
	      break;
	    }
	  else
	    {
	      if ((start = strchr(upper_alpha, s2[i - 1])) &&
		  (finish = strchr(upper_alpha, s2[i + 1])));
	      else if ((start = strchr(lower_alpha, s2[i - 1])) &&
		       (finish = strchr(lower_alpha, s2[i + 1])));
	      else if ((start = strchr(digits, s2[i - 1])) &&
		       (finish = strchr(digits, s2[i + 1])));
	      else
		{
		  fprintf(stderr, "Mismatched operands %c-%c\n", s2[i -  1],
			  s2[i + 1]);
		  s1[j++] = s2[i - 1];
		  s1[j++] = s2[i++];
		  break;
		}
	      p = start;
	      while (p != finish)
		{
		  s1[j++] = *p;
		  if (finish > start)
		    {
		      ++p;
		    }
		  else
		    {
		      --p;
		    }
		}
	      s1[j++] = *p;
	      i += 2;
	    }
	  break;
	default:
	  if (s2[i + 1] == '-' && s2[i + 2] != '\0')
	    {
	      ++i;
	    }
	  else
	    {
	      s1[j++] = s2[i++];
	    }
	  break;
	}
    }
  s1[j] = s2[i];
}
