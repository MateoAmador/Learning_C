#include <stdio.h>

#define maxline 1000

int max;
char line[maxline];

int getlin(void);

int main()
{
  int len;
  extern int max;
  extern char line[maxline];
  while ((len = getlin()) > 0)
    {
      if (len > max)
	{
	  max = len;
	}
      if (max > 0)
	{
	  printf("%s", line);
	}
    }
  return 0;
}

int getlin(void)
{
  int c, i, x;
  x = 0;
  extern char line[];
  
  for (i = 0; i < maxline - 1 && (c=getchar()) != EOF; ++i, ++x)
    {
      if (c == '\t') //turning tabs to a space
	{
	  c = ' ';
	}
      if ((x > 32) && c == ' ')//splits line after at least 32 characters
	{
	  c = '\n';
	  x = 0;
	}
      line[i] = c;
    }
  if (c == '\n')
    {
      line[i] = c;
      ++i;
    }
  line[i] = '\0';
  return i;
}

