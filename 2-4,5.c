#include <stdio.h>

#define size 1000

void getlin1(char x[]);
void getlin2(char x[]);
void squeeze(char x[], char y[]);

int main()
{
  char one[size];
  char two[size];
  getlin1(one);
  getlin2(two);
  squeeze(one, two);
  printf("%s", one);
}


void getlin1(char x[])
{
  int c, i;
  
  for (i = 0; i < size - 1 && (c=getchar()) != EOF && c != '\n'; ++i)
    {
      x[i] = c;
    }
  if (c == '\n')
    {
      x[i] = c;
      ++i;
    }
  x[i] = '\0';
}

void getlin2(char x[])
{
  int c, i;  
  for (i = 0; i < size - 1 && (c=getchar()) != EOF; ++i)
    {
      x[i] = c;
    }
  if (c == '\n')
    {
      x[i] = c;
      ++i;
    }
  x[i] = '\0';
}

void squeeze(char x[], char y[])
{
  int i;
  int z = 0;

  for (i = 0; y[i] != '\0' && x[z] != '\0'; i++, z++)
    {
      if (y[i] == x[z] && y[i] != '\n' && x[z] != '\n')
	{
	  //x[z] = ' ';
	  printf("%d\n", z);
	}
    }
  x[z] = '\0';
}
