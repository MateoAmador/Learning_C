#include <stdio.h>

#define size 100

int mybinsearch(int x, int v[], int n);

int main()
{
  int v[size];//list of numbers
  int x;//returned value
  int n = 1;//what to look for
  int i;

  for (i = 0; i < size; ++i)//creates number list
    {
      v[i] = i;
    }
  
  x = mybinsearch(n, v, size);
  if (x < 0)
    {
      printf("%d not found\n", n);
    }
  else
    {
      printf("%d found at %d\n", n, x);
    }
}

int mybinsearch(int x, int v[], int n)//searches number list, returns index of number in list, return -1 if not found
{
  int low, high, mid;

  low = 0;
  high = n - 1;
  mid = (low + high) / 2;
  while (low <= high && x != v[mid])
    {
      if (x < v[mid])
	{
	  high = mid - 1;
	}
      else
	{
	  low = mid + 1;
	}
      mid = (low + high) / 2;
    }
  if (x == v[mid])
    {
      return mid;
    }
  else
    {
      return -1;
    }
}
